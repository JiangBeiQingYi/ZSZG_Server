package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class EquipData implements PropertyReader {
	
	public int index;
	
	public int number;
	
	public String name;
	
	public int type;
	
	public int star;
	
	public int level;
	
	public String resource;
	
	public String description;
	
	public int sell;
	
	public String icon;
	
	private static HashMap<Integer, EquipData> data = new HashMap<Integer, EquipData>();
	private static List<EquipData> dataList=new ArrayList<EquipData>();
	
	@Override
	public void addData()
	{
		data.put(index, this);
		dataList.add(this);
	}
	
	@Override
	public void resetData()
	{
		data.clear();
	}
	
	@Override
	public void parse(String[] ss)
	{

	}
	
	public static EquipData getData(int equipId)
	{

		return data.get(equipId);
	}
	
	/** ****获得所有装备**** */
	public static List<EquipData> getAllEquipDatas()
	{
		return dataList;
	}
}
