package com.begamer.card.model.pojo;

public class Announce {

	private int id;
	
	private String announce;
	
	private int frequency;//时间间隔:分
	
	private int num;//显示次数
	
	private int type;//类型（2即时性，1事件性） 
	
	private long time;
	
	private long curTime;

	private int aType;//1 游戏内公告   2转盘公告
	
	/**游戏内公告**/
	public static Announce createAnnounce(int id,String context,int frequency,int num,int type)
	{
		Announce announce = new Announce();
		announce.setId(id);
		announce.setAnnounce(context);
		announce.setFrequency(frequency);
		announce.setNum(num);
		announce.setTime(System.currentTimeMillis());
		announce.setCurTime(System.currentTimeMillis());
		announce.setType(type);
		announce.setAType(1);
		return announce;
	}
	/**转盘公告**/
	public static Announce createAnnounce2(int id,String context,int frequency,int num,int type)
	{
		Announce announce = new Announce();
		announce.setId(id);
		announce.setAnnounce(context);
		announce.setFrequency(frequency);
		announce.setNum(num);
		announce.setTime(System.currentTimeMillis());
		announce.setCurTime(System.currentTimeMillis());
		announce.setType(type);
		announce.setAType(2);
		return announce;
	}
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getAnnounce()
	{
		return announce;
	}

	public void setAnnounce(String announce)
	{
		this.announce = announce;
	}

	public int getFrequency()
	{
		return frequency;
	}

	public void setFrequency(int frequency)
	{
		this.frequency = frequency;
	}

	public int getNum()
	{
		return num;
	}

	public void setNum(int num)
	{
		this.num = num;
	}

	public int getType()
	{
		return type;
	}

	public void setType(int type)
	{
		this.type = type;
	}

	public long getTime()
	{
		return time;
	}

	public void setTime(long time)
	{
		this.time = time;
	}

	public long getCurTime()
	{
		return curTime;
	}

	public void setCurTime(long curTime)
	{
		this.curTime = curTime;
	}
	public int getAType() {
		return aType;
	}
	public void setAType(int aType) {
		this.aType = aType;
	}
	
}
