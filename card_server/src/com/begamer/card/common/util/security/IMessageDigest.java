package com.begamer.card.common.util.security;

/**
 * 
 * @ClassName: IMessageDigest
 * @Description: TODO MD5加密
 * @author gs
 * @date Nov 10, 2011 3:57:22 PM
 * 
 */
public interface IMessageDigest {
	/**
	 * 加密：将字符串用某算法加密，返回加密后的字符串
	 * 
	 * @param
	 * @return String
	 * @thorws
	 */
	String digest(String message);

	/**
	 * 加密 ：将字符串加密 并且能够日后解密，返回加密后的字符串
	 * 
	 * @param message
	 * @return
	 */

	String encrypt(String message);

	/**
	 * 解密 ：将encrypt加密的字符串解密，返回解密后的字符串
	 * 
	 * @param message
	 * @return
	 */
	String decryption(String message);
}
