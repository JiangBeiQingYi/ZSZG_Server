package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CardData implements PropertyReader {
	
	public int id;
	
	public int number;
	
	public String name;
	
	public String description;
	
	public int star;
	
	public int maxLevel;
	
	public int race;
	
	public int element;
	
	// ==蓄力声音==//
	public String chargeVoice;
	// ==受伤声音==//
	public String hurtVoice;
	
	public float atk;
	
	public float def;
	
	public float hp;
	
	public int criRate;
	
	public int aviRate;
	
	public int talent;
	public int talent2;
	public int talent3;
	public int talent4;
	
	public float talentpower;
	
	public int type;
	
	public float castTimeDelay;
	
	public int shadow;
	
	public String atlas;
	
	public String cardmodel;
	
	public float modelsize;
	
	public float modelposition;
	
	public float scalenum;
	
	public float modelrotation;
	
	public int sex;
	
	public String icon;
	
	public int sell;
	
	public int basicskill;
	
	public int exp;
	
	public int level;
	
	public String waytoget;
	
	/** 专属技能类型(5,6星卡都拥有): 1随机封印,2反弹,3打掉对方怒气,4受到的专有属性伤害克制一定的伤害值 **/
	public int PSSskilltype1;
	/** 专属技能类型参数1: 0无参数,5任何属性,1风属性,2火属性,3冰属性,4雷属性 **/
	public int PSSparameter11;
	/** 专属技能类型参数2: 克制比例 **/
	public float PSSparameter12;
	/** 专属技能类型参数3 **/
	public int PSSparameter13;
	/** 专属技能类型参数4 **/
	public int PSSparameter14;
	/** 专属技能类型参数5 **/
	public int PSSparameter15;
	/** 专属技能类型概率1 **/
	public int PPSprobability1;
	/** 附加技能类型(6星卡附带技能): 1随机封印,2反弹,3打掉对方怒气,4受到的专有属性伤害克制一定的伤害值 **/
	public int PSSskilltype2;
	/** 附加技能类型参数1：如果类型是1代表封印数量,如果类型是2代表无参数,20无,21风,22火,23,雷,24冰,如果类型是3代表怒气值 **/
	public int PSSparameter21;
	/** 附加技能类型参数2：如果类型是1 就是封印回合数,如果类型是2就是反弹比例,如果类型是3无参数 **/
	public float PSSparameter22;
	/** 附加技能类型参数3 **/
	public int PSSparameter23;
	/** 专属技能类型参数4 **/
	public int PSSparameter24;
	/** 专属技能类型参数5 **/
	public int PSSparameter25;
	/** 专属技能类型概率2 **/
	public int PPSprobability2;
	/** 专属技能图标 **/
	public String PSSicon;
	/** 专属技能特效 **/
	public String PSSeffective;
	/** 专属技能效果 **/
	public String PSSresult;
	/** 专属特效声音 **/
	public String PSSvioce;
	/** 专属特效时间 **/
	public float PSStime;
	/** 专属技能名字 **/
	public String PPSname;
	/** 专属技能描述 **/
	public String PPSdescription;
	
	// /** 5星卡专属技能 **/
	// public String[] fPs;
	// /** 6星卡专属技能 **/
	// public List<String[]> sPs = new ArrayList<String[]>();
	
	private static HashMap<Integer, CardData> data = new HashMap<Integer, CardData>();
	private static List<CardData> dataList = new ArrayList<CardData>();
	
	public void addData()
	{
		data.put(id, this);
		dataList.add(this);
		// String[] ps1 = { PSSskilltype1 + "", PSSparameter11 + "",
		// PSSparameter12 + "", PSSparameter13 + "", PSSparameter14 + "",
		// PSSparameter15 + "" };
		// String[] ps2 = { PSSskilltype2 + "", PSSparameter21 + "",
		// PSSparameter22 + "", PSSparameter23 + "", PSSparameter24 + "",
		// PSSparameter25 + "" };
		// fPs = ps1;
		// sPs.add(ps1);
		// sPs.add(ps2);
	}
	
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}
	
	public void parse(String[] ss)
	{
		
	}
	
	public static CardData getData(int cardId)
	{
		return data.get(cardId);
	}
	
	/** ****所有卡牌**** */
	
	public static List<CardData> getAllCardDatas()
	{
		return dataList;
	}
}
