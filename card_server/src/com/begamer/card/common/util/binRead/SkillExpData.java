package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class SkillExpData implements PropertyReader
{
	//==等级==//
	public int level;
	//==1星经验值==//
	public int starexp1;
	//==2星经验值==//
	public int starexp2;
	//==3星经验值==//
	public int starexp3;
	//==4星经验值==//
	public int starexp4;
	//==5星经验值==//
	public int starexp5;
	public int starexp6;
	//==消耗金币==//
	public int cost;
	
	public int[] starexps;
	
	private static HashMap<Integer, SkillExpData> data=new HashMap<Integer, SkillExpData>();
	public void addData()
	{
		starexps=new int[6];
		starexps[0]=starexp1;
		starexps[1]=starexp2;
		starexps[2]=starexp3;
		starexps[3]=starexp4;
		starexps[4]=starexp5;
		starexps[5]=starexp6;
		data.put(level, this);
	}
	@Override
	public void resetData()
	{
		data.clear();
	}
	@Override
	public void parse(String[] ss)
	{
		
	}
	public static SkillExpData getData(int level)
	{
		return data.get(level);
	}
	
	public static int getExp(int level,int star)
	{
		return data.get(level).starexps[star-1];
	}
}
