package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.begamer.card.common.util.Random;
import com.begamer.card.common.util.StringUtil;
import com.begamer.card.json.CardJson;

public class MazeBattleData implements PropertyReader {
	/**编号**/
	public int id;
	/**大地图**/
	public int map;
	/**类型**/
	public int type;
	/**场景**/
	public int scene;
	/**怪物信息：怪物-等级-攻击力-防御力-生命值-主动技能-boss标识**/
	public String [] monsters;
	/**boss技-回合数**/
	public String [] bossSkill_cd;
	/**人物经验**/
	public int personexp;
	/**卡牌经验**/
	public int cardexp;
	/**掉落金币**/
	public int coins;
	
	private static HashMap<Integer, MazeBattleData> data =new HashMap<Integer, MazeBattleData>(); 
	@Override
	public void addData() {
		data.put(id, this);
	}
	@Override
	public void parse(String[] ss) {
		int location =0;
		id =StringUtil.getInt(ss[location]);
		map =StringUtil.getInt(ss[location+1]);
		type =StringUtil.getInt(ss[location+2]);
		scene =StringUtil.getInt(ss[location+3]);
		monsters =new String [6];
		for(int i=0;i<6;i++)
		{
			location =4+7*i;
			int monster =StringUtil.getInt(ss[location]);
			int level =StringUtil.getInt(ss[location+1]);
			int atk =StringUtil.getInt(ss[location+2]);
			int def =StringUtil.getInt(ss[location+3]);
			int hp=StringUtil.getInt(ss[location+4]);
			int skill =StringUtil.getInt(ss[location+5]);
			int boss =StringUtil.getInt(ss[location+6]);
			String msg =monster+"-"+level+"-"+atk+"-"+def+"-"+hp+"-"+skill+"-"+boss;
			monsters[i] =msg;
		}
		bossSkill_cd =new String[3];
		for(int i=0;i<3;i++)
		{
			location =46+2*i;
			int bossSkill =StringUtil.getInt(ss[location]);
			int cd =StringUtil.getInt(ss[location+1]);
			String msg =bossSkill+"-"+cd;
			bossSkill_cd[i] =msg;
		}
		location =52;
		personexp =StringUtil.getInt(ss[location]);
		cardexp =StringUtil.getInt(ss[location+1]);
		coins =StringUtil.getInt(ss[location+2]);
		addData();
	}

	@Override
	public void resetData() {
		data.clear();
	}

	public static MazeBattleData getMazeBattleData(int mazeBattleId)
	{
		return data.get(mazeBattleId);
	}
	
	public static MazeBattleData getMazeBattleDataRandom(int map ,int type)
	{
		List<MazeBattleData> list =new ArrayList<MazeBattleData>();
		for(MazeBattleData mData : data.values())
		{
			if(mData.map==map && mData.type==type)
			{
				list.add(mData);
			}
		}
		int random=0;
		MazeBattleData m =null;
		if(list !=null && list.size()>0)
		{
			random =Random.getNumber(0, list.size());
			 m =list.get(random);
		}
		return m;
	}
	
	public static CardJson[] getMonstersData(int td)
	{
		MazeBattleData mbdata=MazeBattleData.getMazeBattleData(td);
		if(mbdata==null)
		{
			return null;
		}
		CardJson[] cjs=new CardJson[6];
		for(int i=0;i<6;i++)
		{
			String[] ss=mbdata.monsters[i].split("-");
			int cardId=StringUtil.getInt(ss[0]);
			int level=StringUtil.getInt(ss[1]);
			int atk=StringUtil.getInt(ss[2]);
			int def=StringUtil.getInt(ss[3]);
			int maxHp=StringUtil.getInt(ss[4]);
			int skillId=StringUtil.getInt(ss[5]);
			int bossMark=StringUtil.getInt(ss[6]);
			
			int criRate=0;
			int aviRate=0;
			int talent1=0;
			int talent2=0;
			int talent3=0;
			
			if(CardData.getData(cardId)==null)
			{
				continue;
			}
			cjs[i]=new CardJson(i, cardId, level, skillId, atk, def, maxHp, bossMark, criRate, aviRate,talent1, talent2, talent3);
		}
		return cjs;
	}
}
