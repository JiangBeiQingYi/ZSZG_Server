package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class PvpCdCostData implements PropertyReader
{
	public int number;
	public int viplevel;
	public int cost;
	
	private static HashMap<Integer, PvpCdCostData> data =new HashMap<Integer, PvpCdCostData>();
	@Override
	public void addData()
	{
		data.put(number, this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static PvpCdCostData getPvpCdCostData(int index)
	{
		return data.get(index);
	}
	
	public static Integer getDataSize()
	{
		return data.size();
	}
}
