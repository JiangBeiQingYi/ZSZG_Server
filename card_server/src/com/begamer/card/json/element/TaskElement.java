package com.begamer.card.json.element;

import java.util.List;

public class TaskElement
{
	public int id;
	public String name;//名称
	public String icon;//头像
	public String description;//描述
	public List<String> reward;//奖励类型-id
	public int type;//是否完成 1完成未领取  0未完成 2 领取过奖励
	public int num;//已完成次数
	public int sNum;//目标次数
	public int unlockLevel;//解锁等级
	public String ulDesc;//解锁描述
	public int activeNum;//活跃度
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<String> getReward() {
		return reward;
	}
	public void setReward(List<String> reward) {
		this.reward = reward;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getSNum() {
		return sNum;
	}
	public void setSNum(int sNum){
		this.sNum = sNum;
	}
	public int getUnlockLevel() {
		return unlockLevel;
	}
	public void setUnlockLevel(int unlockLevel) {
		this.unlockLevel = unlockLevel;
	}
	public String getUlDesc() {
		return ulDesc;
	}
	public void setUlDesc(String ulDesc) {
		this.ulDesc = ulDesc;
	}
	public int getActiveNum() {
		return activeNum;
	}
	public void setActiveNum(int activeNum) {
		this.activeNum = activeNum;
	}
	
	
}
