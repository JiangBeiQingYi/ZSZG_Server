package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.ShopElement;

public class ShopUiResultJson extends ErrorJson
{
	public List<ShopElement> ses1;//商品信息s  id-num(购买次数)
	public List<ShopElement> ses2;//商品信息s id-snum(购买总次数)

	public int refreshTime;//距离刷新的时间(单位s)
	public int refresh;//刷新次数
	public int pvpHonor;//pvp荣誉值
	public List<ShopElement> getSes1() {
		return ses1;
	}

	public void setSes1(List<ShopElement> ses1) {
		this.ses1 = ses1;
	}

	public List<ShopElement> getSes2() {
		return ses2;
	}

	public void setSes2(List<ShopElement> ses2) {
		this.ses2 = ses2;
	}

	public int getRefreshTime() {
		return refreshTime;
	}

	public void setRefreshTime(int refreshTime) {
		this.refreshTime = refreshTime;
	}

	public int getRefresh() {
		return refresh;
	}

	public void setRefresh(int refresh) {
		this.refresh = refresh;
	}

	public int getPvpHonor() {
		return pvpHonor;
	}

	public void setPvpHonor(int pvpHonor) {
		this.pvpHonor = pvpHonor;
	}
	
}
