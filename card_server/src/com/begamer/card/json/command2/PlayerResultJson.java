package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PlayerElement;

public class PlayerResultJson extends ErrorJson
{
	public List<PlayerElement> list;
	//解锁模块     id-是否解锁(0未解锁，1解锁)
	public String [] s;
	public int mark;//==0不需重新登录,1需要重新登录==//
	
	public List<PlayerElement> getList()
	{
		return list;
	}

	public void setList(List<PlayerElement> list)
	{
		this.list = list;
	}

	public String[] getS()
	{
		return s;
	}

	public void setS(String[] s)
	{
		this.s = s;
	}

	public int getMark()
	{
		return mark;
	}

	public void setMark(int mark)
	{
		this.mark = mark;
	}
	
}
