package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.element.ShopElement;

public class RunnerResultJson extends ErrorJson
{
	public int type;//转动获得物品的类型
	public String id;//转动获得物品的id,数量
	public List<ShopElement> list;//累计转动n次获得物品是否领取标识
	public int index;
	public List<ShopElement> view;//显示转盘次数
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<ShopElement> getList() {
		return list;
	}
	public void setList(List<ShopElement> list) {
		this.list = list;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public List<ShopElement> getView()
	{
		return view;
	}
	public void setView(List<ShopElement> view)
	{
		this.view = view;
	}
	
}
