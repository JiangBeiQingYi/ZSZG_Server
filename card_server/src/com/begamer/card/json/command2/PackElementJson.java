package com.begamer.card.json.command2;

import com.begamer.card.json.PackElement;

public class PackElementJson{
	public PackElement pe;
	public int type;//请求阵容ui4时，1为有提醒 , 0为没有新提醒  , 2为提升；请求背包界面时，表示装备或者技能所在卡的id,(如果装备或者技能不在卡组中，或者请求背包中的其他物品时，则默认为0，)
	
	public PackElement getPe() {
		return pe;
	}
	public void setPe(PackElement pe) {
		this.pe = pe;
	}
	public int getType() {
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	
}
