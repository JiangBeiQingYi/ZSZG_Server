package com.begamer.card.json.command2;



/**
 * @ClassName: GiftJson
 * @author gs
 * @date 2014-5-20 下午09:27:48
 * 
 */
public class GiftJson {
	public int result; // 0激活码不对 1成功 2玩家已领过,3激活码已使用
	public int gold;//金币
	public int crystal;//水晶
	public int runeNum;//符文值
	public int power;//体力
	public String card;//格式:cardId-num
	public String skill;//主动技能:skillId-num
	public String pSkill;//被动技能:pSkillId-num
	public String equip;//装备:equipId-num
	public String item;//材料:itemId-num
	
	public int getResult()
	{
		return result;
	}
	public void setResult(int result)
	{
		this.result = result;
	}
	public int getGold()
	{
		return gold;
	}
	public void setGold(int gold)
	{
		this.gold = gold;
	}
	public int getCrystal()
	{
		return crystal;
	}
	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}
	public int getRuneNum()
	{
		return runeNum;
	}
	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}
	public int getPower()
	{
		return power;
	}
	public void setPower(int power)
	{
		this.power = power;
	}
	public String getCard()
	{
		return card;
	}
	public void setCard(String card)
	{
		this.card = card;
	}
	public String getSkill()
	{
		return skill;
	}
	public void setSkill(String skill)
	{
		this.skill = skill;
	}
	public String getpSkill()
	{
		return pSkill;
	}
	public void setpSkill(String pSkill)
	{
		this.pSkill = pSkill;
	}
	public String getEquip()
	{
		return equip;
	}
	public void setEquip(String equip)
	{
		this.equip = equip;
	}
	public String getItem()
	{
		return item;
	}
	public void setItem(String item)
	{
		this.item = item;
	}
}
