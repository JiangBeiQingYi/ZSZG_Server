package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class FriendSearchJson extends BasicJson
{
	public String name;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
}
