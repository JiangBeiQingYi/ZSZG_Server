package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class ExchangeResultJson extends ErrorJson{

	public int id;
	public int curNeedNum;// 玩家当前拥有兑换物品所要的物品个数
	public int sell;// 是否可以兑换(0不能兑换,1兑换,3物品不足)
	
	
	public int getSell()
	{
		return sell;
	}
	public void setSell(int sell)
	{
		this.sell = sell;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getCurNeedNum()
	{
		return curNeedNum;
	}
	public void setCurNeedNum(int curNeedNum)
	{
		this.curNeedNum = curNeedNum;
	}
}
