package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class LotJson extends BasicJson
{
	/**
	 * 抽卡方式:
	 * 1友情抽卡
	 * 2钻石抽卡
	 * 3钻石十连抽
	 * 4魔法抽卡
	 * 5女神卡
	 * 6北欧卡
	 * 7希腊卡
	 * 8东亚卡
	 * 9中国卡
	 * 10至尊包
	 * 11抽卡排行榜活动免费抽取
	 * 12抽卡排行榜活动钻石抽取
	 */
	public int t;
	public int cs;

	public int getCs()
	{
		return cs;
	}

	public void setCs(int cs)
	{
		this.cs = cs;
	}

	public int getT()
	{
		return t;
	}

	public void setT(int t)
	{
		this.t = t;
	}
	
}
