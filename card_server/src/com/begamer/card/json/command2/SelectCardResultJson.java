package com.begamer.card.json.command2;

import java.util.List;

import com.begamer.card.json.ErrorJson;
import com.begamer.card.json.PackElement;

public class SelectCardResultJson extends ErrorJson
{
	public List<PackElement> pes;
	/**技能id**/
	public int i;
	public List<PackElement> getPes() {
		return pes;
	}
	public void setPes(List<PackElement> pes) {
		this.pes = pes;
	}
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
}
