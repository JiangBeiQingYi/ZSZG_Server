package com.begamer.card.common;

/**
 * @author LiTao
 * 2014-5-9 上午11:06:29
 */
public class Constant {

    public static final Integer LOAD_ALL = new Integer(0);
    //验证码图片session
    public static final String IMAGE_SESSION = "IMAGE_RAND";
    //登录用户session
    public static final String LOGIN_USER = "user";
    //领取礼包密码
    public static final String GiftPassword="card3bwlanD";
    //分页查询每页尺寸
    public static final int PageSize=20;
    
    public static final String SIGNATURE = "sign";

	public static final String CHARSET = "UTF-8";
	
	public static final int TbOrder=1000000000;
}
