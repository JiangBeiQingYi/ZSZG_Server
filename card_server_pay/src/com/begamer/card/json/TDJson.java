package com.begamer.card.json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.begamer.card.model.pojo.PayInfo;

public class TDJson
{
	/**该条数据的ID,由开发者自行定义;唯一标识你发来的一条数据**/
	public String msgID;
	/**充值状态.标识这是一次充值请求request,还是已经成功完成支付success**/
	public String status;
	/**玩家此次充值使用的设备系统平台:ios/android**/
	public String OS;
	/**玩家账户ID**/
	public String accountID;
	/**订单ID**/
	public String orderID;
	/**充值金额**/
	public double currencyAmount;
	/**货币类型:人民币CNY,美元USD,欧元EUR**/
	public String currencyType;
	/**充值获得的虚拟币额度**/
	public double virtualCurrencyAmount;
	
	//==================以上为必填项=====================================================================//
	
	/**玩家充值时间,13位毫秒数**/
	public long chargeTime;
	/**玩家购买的充值包类型,如6000元宝大包箱**/
	public String iapID;
	/**支付方式:如支付宝,苹果iap,银联**/
	public String paymentType;
	/**玩家充值的区服**/
	public String gameServer;
	/**游戏客户端版本号**/
	public String gameVersion;
	/**玩家充值时的等级**/
	public int level;
	/**玩家充值时所在的关卡或任务**/
	public String mission;
	
	
	public static TDJson[] createArray(PayInfo pi)
	{
		TDJson[] array=new TDJson[1];
		array[0]=createSuccessTdElement(pi);
		return array;
	}
	
	@SuppressWarnings("unused")
	private static TDJson createRequestTdElement(PayInfo pi)
	{
		PayExtraJson pej=JSON.toJavaObject(JSON.parseObject(pi.getExtraData()), PayExtraJson.class);
		
		TDJson tde=new TDJson();
		tde.setMsgID(pi.getId()+"");
		tde.setStatus("request");//request/success
		tde.setOS(pej.getOs());
		tde.setAccountID(pej.getUsername());
		tde.setOrderID(pi.getConsumeId());
		tde.setCurrencyAmount(Double.valueOf(pi.getConsumeValue()));
		tde.setCurrencyType("CNY");
		tde.setVirtualCurrencyAmount(0);
		//==================以上为必填项=====================================================================//
		tde.setChargeTime(pi.getReqtime());
		tde.setIapID(pej.rechargeId+"");
		tde.setGameServer(pi.getGameServerZone());
		return tde;
	}
	
	private static TDJson createSuccessTdElement(PayInfo pi)
	{
		PayExtraJson pej=JSON.toJavaObject(JSON.parseObject(pi.getExtraData()), PayExtraJson.class);
		
		TDJson tde=new TDJson();
		tde.setMsgID(pi.getId()+"");
		tde.setStatus("success");//request/success
		tde.setOS(pej.getOs());
		tde.setAccountID(pej.getUsername());
		tde.setOrderID(pi.getConsumeId());
		tde.setCurrencyAmount(Double.valueOf(pi.getConsumeValue()));
		tde.setCurrencyType("CNY");
		tde.setVirtualCurrencyAmount(0);
		//==================以上为必填项=====================================================================//
		tde.setChargeTime(pi.getReqtime());
		tde.setIapID(pej.rechargeId+"");
		tde.setGameServer(pi.getGameServerZone());
		return tde;
	}
	
	public String getMsgID()
	{
		return msgID;
	}
	public void setMsgID(String msgID)
	{
		this.msgID = msgID;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	@JSONField(name="OS")
	public String getOS()
	{
		return OS;
	}
	public void setOS(String OS)
	{
		this.OS = OS;
	}
	public String getAccountID()
	{
		return accountID;
	}
	public void setAccountID(String accountID)
	{
		this.accountID = accountID;
	}
	public String getOrderID()
	{
		return orderID;
	}
	public void setOrderID(String orderID)
	{
		this.orderID = orderID;
	}
	public double getCurrencyAmount()
	{
		return currencyAmount;
	}
	public void setCurrencyAmount(double currencyAmount)
	{
		this.currencyAmount = currencyAmount;
	}
	public String getCurrencyType()
	{
		return currencyType;
	}
	public void setCurrencyType(String currencyType)
	{
		this.currencyType = currencyType;
	}
	public double getVirtualCurrencyAmount()
	{
		return virtualCurrencyAmount;
	}
	public void setVirtualCurrencyAmount(double virtualCurrencyAmount)
	{
		this.virtualCurrencyAmount = virtualCurrencyAmount;
	}
	public long getChargeTime()
	{
		return chargeTime;
	}
	public void setChargeTime(long chargeTime)
	{
		this.chargeTime = chargeTime;
	}
	public String getIapID()
	{
		return iapID;
	}
	public void setIapID(String iapID)
	{
		this.iapID = iapID;
	}
	public String getPaymentType()
	{
		return paymentType;
	}
	public void setPaymentType(String paymentType)
	{
		this.paymentType = paymentType;
	}
	public String getGameServer()
	{
		return gameServer;
	}
	public void setGameServer(String gameServer)
	{
		this.gameServer = gameServer;
	}
	public String getGameVersion()
	{
		return gameVersion;
	}
	public void setGameVersion(String gameVersion)
	{
		this.gameVersion = gameVersion;
	}
	public int getLevel()
	{
		return level;
	}
	public void setLevel(int level)
	{
		this.level = level;
	}
	public String getMission()
	{
		return mission;
	}
	public void setMission(String mission)
	{
		this.mission = mission;
	}
}
