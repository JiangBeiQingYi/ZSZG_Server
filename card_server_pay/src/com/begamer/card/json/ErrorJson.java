package com.begamer.card.json;

import com.alibaba.fastjson.annotation.JSONField;

public class ErrorJson
{
	public String ErrorCode;//错误码(0=失败，1=成功（已处理过的，也当作成功返回），2= AppId无效，3= Act 无效，4=参数无效，5= Sign 无效，其他的应用自己定义，并在错误描述中体现)
	public String ErrorDesc;//错误描述
	@JSONField(name="ErrorCode")
	public String getErrorCode()
	{
		return ErrorCode;
	}
	@JSONField(name="ErrorCode")
	public void setErrorCode(String errorCode)
	{
		ErrorCode = errorCode;
	}
	@JSONField(name="ErrorDesc")
	public String getErrorDesc()
	{
		return ErrorDesc;
	}
	@JSONField(name="ErrorDesc")
	public void setErrorDesc(String errorDesc)
	{
		ErrorDesc = errorDesc;
	}
}
